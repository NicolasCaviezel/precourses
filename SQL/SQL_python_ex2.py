import matplotlib.pyplot as plt
import mysql.connector
import numpy as np
from matplotlib.backends.backend_pdf import PdfPages

PATH = "/Users/nico/Documents/Cours/ITC/PreCourses/SQL"

def remove_list(list1, list2):
    Mylist=[x for x in list2 if x not in list1]
    return Mylist

def tuple_to_list(list_of_tuple):
    Mylist=[]
    for i in range(len(list_of_tuple)):
        Mylist.append(list_of_tuple[i][0])
    return Mylist

username = 'root'

cnx = mysql.connector.connect(host='127.0.0.1', user=username, passwd='BIShamsa5!', db='imdb')

querry1= "SELECT DISTINCT actor_id FROM cast WHERE movie_id IN \
        (SELECT movie_id FROM cast WHERE \
        actor_id=(SELECT id FROM actors WHERE full_name='Kevin Bacon'))"
cursor=cnx.cursor()

try:
    cursor.execute(querry1)
    # Fetch all the rows in a list of lists.
    res_level1 = cursor.fetchall()
    # results are in an array containing the content of your query.
    # Use it as you wish ...
    res_level1=tuple_to_list(res_level1)
except:
    print "Error: unable to fecth data"         
    
querry2="SELECT DISTINCT actor_id FROM cast WHERE movie_id IN \
        (SELECT movie_id FROM cast WHERE actor_id IN \
        (" + ",".join(map(str, res_level1)) + "))"
 
try:
    cursor.execute(querry2)
    res_level2 = cursor.fetchall()
    res_level2 = tuple_to_list(res_level2)
    res_level2 = remove_list(res_level1, res_level2)
    
except: 
    print "Error: unable to fecth data"
  
querry3="SELECT DISTINCT actor_id FROM cast WHERE movie_id IN \
        (SELECT movie_id FROM cast WHERE actor_id IN \
        (" + ",".join(map(str, res_level2)) + "))"
 
try:
    cursor.execute(querry3)
    res_level3 = cursor.fetchall()
    res_level3 = tuple_to_list(res_level3)
    res_level3 = remove_list(res_level1, res_level3)
    res_level3 = remove_list(res_level2, res_level3)
    
except: 
    print "Error: unable to fecth data"   

querry4="SELECT DISTINCT actor_id FROM cast WHERE movie_id IN \
        (SELECT movie_id FROM cast WHERE actor_id IN \
        (" + ",".join(map(str, res_level3)) + "))"
 
try:
    cursor.execute(querry4)
    res_level4 = cursor.fetchall()
    res_level4 = tuple_to_list(res_level4)
    res_level4 = remove_list(res_level1, res_level4)
    res_level4 = remove_list(res_level2, res_level4)
    res_level4 = remove_list(res_level3, res_level4)
                             
except: 
    print "Error: unable to fecth data"   
 
querry5="SELECT DISTINCT actor_id FROM cast WHERE movie_id IN \
        (SELECT movie_id FROM cast WHERE actor_id IN \
        (" + ",".join(map(str, res_level4)) + "))"
 
try:
    cursor.execute(querry5)
    res_level5 = cursor.fetchall()
    res_level5 = tuple_to_list(res_level5)
    res_level5 = remove_list(res_level1, res_level5)
    res_level5 = remove_list(res_level2, res_level5)
    res_level5 = remove_list(res_level3, res_level5)
    res_level5 = remove_list(res_level4, res_level5)
                             
except: 
    print "Error: unable to fecth data"   
    
act1=len(res_level1)
act2=len(res_level2)
act3=len(res_level3)
act4=len(res_level4)    
act5=len(res_level5)  
print(act1, act2, act3, act4, act5)

cnx.close()     

X=[1, 2,3 ,4 ,5, 6]
Y=[act1, act2, act3, act4, act5, 0]
plt.bar(X, Y)
plt.xlabel('degrees')
plt.ylabel('actors')
plt.legend()
plt.savefig(PATH)

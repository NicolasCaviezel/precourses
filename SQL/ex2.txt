CREATE DATABASE myDB;

1) CREATE TABLE food (
	ID INT PRIMARY KEY,
	Name VARCHAR(20) NOT NULL,
	Do_I_like_it BIT(1);
	
2)	INSERT INTO food VALUES 
	(1, 'pizza', 1), (2, 'broccoli', 0), (3, 'icecream', 1);

3)	UPDATE food SET Do_I_like_it = 1 WHERE ID=2 LIMIT 1;

4) 	DELETE FROM food WHERE  ID=2 LIMIT 1;
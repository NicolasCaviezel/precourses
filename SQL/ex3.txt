1) SELECT * FROM actors ORDER BY full_name;
		the table ID|full_name|gender appears, order by name
		2000 rows in the table
		
2) SELECT COUNT(id) FROM movies;
		There are 38 movies
		
3)  SELECT COUNT(id) FROM movies WHERE genre='Action';
		There are 3 action movies
		
4)  SELECT COUNT(id) FROM movies WHERE genre!='Action' OR genre IS NULL;
		There are 34 action movies
		The sum makes 37 and not 38 because there is 'fake movies' which has no genre specified

5)  SELECT COUNT(id), year FROM movies GROUP BY year;
		A 2 columns table appears, one with movies' year and the second with the number of movies this year
		
6)	SELECT * FROM movies WHERE title like '% the%';
		There are 6 movies with 'the' in their title 